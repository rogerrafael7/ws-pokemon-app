const fs = require('fs');
const path = require('path');
const dotenv = require('dotenv');

module.exports = () => {
  let args = process.argv;
  let index = args.findIndex((arg) => new RegExp('--ENV').test(arg));
  let ENV = 'DEV';

  if (~index) {
    ENV = args[index].match(/--ENV=(.+)/i);
    if (ENV) {
      ENV = ENV[1];
    }
  }

  process.env.ENV = ENV;
  let fileStr = fs.readFileSync(path.resolve(__dirname, './', ENV.toLowerCase() + '.env')).toString();
  fs.writeFileSync(path.resolve(__dirname, '../', '.env'), fileStr);
  dotenv.config();
};