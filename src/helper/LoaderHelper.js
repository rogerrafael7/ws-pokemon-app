const fs = require('fs');
const path = require('path');

class LoaderHelper {

  static loadFiles(PATH) {
    let filesJS = fs
      .readdirSync(path.resolve(PATH))
      .filter((fileName) => /\.js$/.test(fileName));

    let modulesByFileName = {};

    for (let fileName of filesJS) {
      fileName = fileName.replace('\.js', '');
      modulesByFileName[fileName] = require(path.resolve(PATH, fileName));
    }

    return modulesByFileName;
  }

}

module.exports = LoaderHelper;