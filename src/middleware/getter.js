class Getter {
  constructor(request, response) {
    this.request = request;
    this.response = response;
  }
  get(paramName) {
    let param;
    if (this.request.params && paramName in this.request.params) {
      param = this.request.params[paramName];
    } else if (this.request.query && paramName in this.request.query) {
      param = this.request.query[paramName];
    } else if (this.request.params && paramName in this.request.params) {
      param = this.request.params[paramName];
    } else if (this.request.body && paramName in this.request.body) {
      param = this.request.body[paramName];
    }
    return param;
  }
}

module.exports = (request, response, next) => {
  request.getter = new Getter(request, response);
  next();
};