const ServerException = require('../_base/ServerException');
class Sender {
  constructor(request, response) {
    this.request = request;
    this.response = response;
  }

  error(error, status = 400) {
    try {
      if (error instanceof ServerException) {
        if (error.$status) {
          status = error.$status;
        }
        throw new Error(error.$message);
      }

      if (error.errors instanceof Array) {
        throw new Error(error.errors.map(({message}) => message).join(', '));
      }

      console.error(error);
      throw new Error(error.message || 'Erro desconhecido! Contacte o Administrador.');
    } catch ({message}) {
      this.response.status(status);
      this.response.json({ message });
    }
  }

  end(result, contentType = 'application/json') {
    try {
      this.response.header('content-type', contentType);
      let contentTypeMap = {
        'application/json' : (result) => {
          this.response.json(result);
        }
      };
      if (contentTypeMap[contentType]) {
        contentTypeMap[contentType](result);
      } else {
        this.response.end(result);
      }
    } catch (error) {
      this.error(error);
    }
  }

}

module.exports = (request, response, next) => {
  response.sender = new Sender(request, response);
  next();
};