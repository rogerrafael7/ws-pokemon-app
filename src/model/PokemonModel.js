const {TipoPokemon} = require("../constant/TipoPokemon");
const { INTEGER, STRING, ENUM } = require("sequelize");

exports.register = ({sequelize}) => {
  exports.PokemonModel = sequelize.define('tb_pokemons', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tipo: {
      type: ENUM(Object.keys(TipoPokemon).map((key) => TipoPokemon[key])),
      allowNull: false
    },
    treinador: {
      type: STRING,
      allowNull: false
    },
    // nao mapeado nas rotas
    nivel: {
      type: INTEGER,
      allowNull: false,
      defaultValue: 1
    }
  }, {
    schema: process.env.DB_SCHEMA
  })
};