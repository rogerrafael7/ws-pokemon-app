const HttpVerb = {
  GET : 'get',
  POST: 'post',
  PATCH: 'patch',
  DELETE: 'delete',
  PUT: 'put'
};
module.exports = HttpVerb;