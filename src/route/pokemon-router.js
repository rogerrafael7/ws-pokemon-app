const HttpVerb = require("../constant/HttpVerb");
const PokemonController = require("../controller/PokemonController");

const pokemonRouter = [
  {
    route: '/pokemons',
    verbs: [HttpVerb.GET],
    tags: ["pokemons"],
    summary: " ",
    description: " ",
    response: {
      "200": {
        description: "Operação realizada com sucesso.",
        schema: {
          content: {
            type: "array",
            items: {
              type: "object",
              description: "",
              properties: {

              }
            }
          }
        }
      },
      "400": {
        description: "Erro no consumo",
      }
    },
    params: [],
    Controller: PokemonController,
    declaredMethod: 'getPokemons'
  },
  {
    route: '/pokemons/:id',
    verbs: [HttpVerb.GET],
    tags: ["pokemons"],
    summary: " ",
    description: " ",
    response: {
      "200": {
        description: "Operação realizada com sucesso.",
        schema: {
          content: {
            type: "array",
            items: {
              type: "object",
              description: "",
              properties: {}
            }
          }
        }
      },
      "400": {
        description: "Erro no consumo",
      }
    },
    params: [],
    Controller: PokemonController,
    declaredMethod: 'getPokemonById'
  },
  {
    route: '/pokemons',
    verbs: [HttpVerb.POST],
    tags: ["pokemon"],
    summary: " ",
    description: " ",
    response: {
      "200": {
        description: "Operação realizada com sucesso.",
        schema: {
          content: {
            type: "array",
            items: {
              type: "object",
              description: "",
              properties: {
                "id": { type: 'number' },
                "tipo":  { type: 'string' },
                "treinador":  { type: 'string' },
                "nivel":  { type: 'number' }
              }
            }
          }
        }
      },
      "400": {
        description: "Erro no consumo",
      }
    },
    params: [
      {
        in: "body",
        name: "body",
        description: "Parâmetros para inserção de pokemons",
        schema: {
          type: "object",
          properties: {
            tipo: {
              required: true,
              type: "string",
              example: "pikachu",
              description: "Identificação do tipo de Pokemon"
            },
            treinador: {
              required: true,
              type: "string",
              example: "Roger Rafael",
              description: "Nome do treinador"
            }
          }
        }
      }
    ],
    Controller: PokemonController,
    declaredMethod: 'savePokemon'
  },
  {
    route: '/pokemons/:id',
    verbs: [HttpVerb.PUT],
    tags: ["pokemon"],
    summary: " ",
    description: " ",
    response: {
      "204": {
        description: "No Content",
        schema: {}
      },
      "400": {
        description: "Erro no consumo",
      }
    },
    params: [
      {
        in: "body",
        name: "body",
        description: "Parâmetros para edição de pokemons",
        schema: {
          type: "object",
          properties: {
            treinador: {
              required: true,
              type: "string",
              example: "Roger Rafael",
              description: "Nome do treinador"
            }
          }
        }
      }
    ],
    Controller: PokemonController,
    declaredMethod: 'editPokemon'
  },
  {
    route: '/pokemons/:id',
    verbs: [HttpVerb.DELETE],
    tags: ["pokemon"],
    summary: " ",
    description: " ",
    response: {
      "204": {
        description: "No Content",
        schema: {}
      },
      "400": {
        description: "Erro no consumo",
      }
    },
    params: [],
    Controller: PokemonController,
    declaredMethod: 'deletePokemon'
  },
  {
    route: '/batalhar/:pokemonAId/:pokemonBId',
    verbs: [HttpVerb.POST],
    tags: ["pokemon"],
    summary: " ",
    description: " ",
    response: {
      "200": {
        description: "",
        schema: {
          content: {
            type: "object",
            items: {
              type: "object",
              description: "",
              properties: {
                vencedor: {
                  id: { type: 'number' },
                  tipo: { type: 'string' },
                  treinador: { type: 'string' },
                  nivel: { type: 'number' },
                },
                perdedor: {
                  id: { type: 'number' },
                  tipo: { type: 'string' },
                  treinador: { type: 'string' },
                  nivel: { type: 'number' },
                }
              }
            }
          }
        }
      },
      "400": {
        description: "Erro no consumo",
      }
    },
    params: [],
    Controller: PokemonController,
    declaredMethod: 'batalhar'
  },
];

module.exports = pokemonRouter;