const {TipoPokemon} = require("../constant/TipoPokemon");
const ServerException = require("../_base/ServerException");
const ControllerBase = require("./_base/ControllerBase");
const pokemonService = require("../service/PokemonService");

class PokemonController extends ControllerBase {

  async batalhar({}, { pokemonAId, pokemonBId }) {
    if (pokemonAId === pokemonBId) {
      throw new ServerException('A batalha deve ser feita por pokemons diferentes!');
    }
    return pokemonService.batalhar(pokemonAId, pokemonBId);
  }

  async getPokemons() {
    return pokemonService.getPokemons();
  }

  async getPokemonById({}, { id }) {
    return pokemonService.getPokemonById(id);
  }

  async savePokemon(pokemon) {
    let typesAllowed = Object.keys(TipoPokemon).map((key) => TipoPokemon[key]);
    if (!~typesAllowed.indexOf(pokemon.tipo)) {
      throw new ServerException(`Apenas os tipos: ${typesAllowed.join(', ')} são aceitos`);
    }
    return pokemonService.savePokemon(pokemon);
  }

  async editPokemon({ treinador }, { id }) {
    await pokemonService.editPokemon({ treinador }, { id });
  }

  async deletePokemon({}, { id }) {
    await pokemonService.deletePokemon(id);
  }

}


module.exports = PokemonController;