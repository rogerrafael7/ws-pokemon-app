class ControllerBase {

  constructor(request, response) {
    this.request = request;
    this.response = response;
  }

}

module.exports = ControllerBase;