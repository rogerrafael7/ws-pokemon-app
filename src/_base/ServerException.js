class ServerException extends Error{
  constructor(errorMessage, status) {
    super(errorMessage);
    this.$message = errorMessage;
    this.$status = status;
  }
}
module.exports = ServerException;