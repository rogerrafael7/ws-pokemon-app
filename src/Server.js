const express = require('express');
const serverConfig = require('./serverConfig');
const Sequelize = require('sequelize');
const bodyParser = require('body-parser');
const ModelLoader = require('./loader/ModelLoader');
const RouterLoader = require('./loader/RouterLoader');
const sender = require('./middleware/sender');
const getter = require('./middleware/getter');

class Server {
  constructor(port) {
    this.port = port;
    this.app = express();
    this.start();
  }

  async start() {
    try {
      this.registerMiddlewares();
      await this.connectDatabase();
      await this.registerRoutes();
      this.app.listen(this.port, () => {
        console.log('Server pronto e rodando na porta: ', this.port, ' | ENV:', process.env.ENV);
      });
    } catch (error) {
      console.error(error);
    }
  }

  registerMiddlewares() {
    this.app.use(sender);
    this.app.use(getter);

    this.app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', req.headers.origin);
      res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
      res.header("Access-Control-Max-Age", "3600");
      res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
      res.header("Access-Control-Allow-Credentials", "true");

      if ('OPTIONS' === req.method) {
        res.sendStatus(200);
        return res.end();
      } else {
        next();
      }
    });
    this.app.use(bodyParser.json());
  }

  async connectDatabase() {
    let { db } = serverConfig;
    const sequelize = new Sequelize(db.database, db.username, db.password, db);
    await sequelize.authenticate();
    await this.registerModels(sequelize);
    await this.modelLoader.registerRelationships(sequelize);
    await sequelize.sync({ logging: false });
  }

  async registerModels(sequelize) {
    this.modelLoader = new ModelLoader(sequelize);
    await this.modelLoader.load();
  }

  async registerRoutes() {
    this.routerLoader = new RouterLoader();
    await this.routerLoader.load();
  }

}

module.exports = Server;