const Server = require('./Server');
const OS = require('os');
const cluster = require('cluster');

const initServer = (CLUSTER = false) => {
  let instanceServer;
  let port = process.env.PORT || process.env.ENV_PORT;
  let countNodes = process.env.WEB_CONCURRENCY || OS.cpus().length;

  if (CLUSTER) {
    if (cluster.isMaster) {
      let i = 0;

      while (i < countNodes) {
        cluster.fork();
        i += 1;
      }

    } else {
      instanceServer = new Server(port);
    }

  } else {
    instanceServer = new Server(port);
  }

  global.server = instanceServer;
};

initServer(process.env.ENV_CLUSTER || false);