const {PokemonModel} = require("../model/PokemonModel");
const ServerException = require("../_base/ServerException");

class PokemonService {

  async batalhar(pokemonAId, pokemonBId) {
    let [pokemonA, pokemonB] = await Promise.all([
      this.getPokemonById(pokemonAId),
      this.getPokemonById(pokemonBId),
    ]);

    if (!pokemonA) {
      throw new ServerException(`O pokemon A de ID: ${pokemonAId} não existe`);
    }

    if (!pokemonB) {
      throw new ServerException(`O pokemon B de ID: ${pokemonBId} não existe`);
    }

    let weightA = 10;
    let weightB = 10;

    if (pokemonA.nivel > pokemonB.nivel) {
      weightA = weightA * 1.066;
    } else if (pokemonB.nivel > pokemonA.nivel) {
      weightB = weightB * 1.066;
    }

    let forceA = Math.ceil(Math.random() * pokemonA.nivel + weightA);
    let forceB = Math.ceil(Math.random() * pokemonA.nivel + weightB);

    console.log('forceA:', forceA, ' | forceB:',  forceB);

    let vencedor;
    let perdedor;

    if (forceA > forceB) {
      vencedor = pokemonA;
      perdedor = pokemonB;
    } else {
      vencedor = pokemonB;
      perdedor = pokemonA;
    }

    vencedor.nivel += 1;
    perdedor.nivel -= 1;

    await await PokemonModel.update(
      { nivel: vencedor.nivel },
      { where: { id: vencedor.id } }
    );

    if (perdedor.nivel === 0) {
      await this.deletePokemon(perdedor.id);
    } else {
      await await PokemonModel.update(
        { nivel: perdedor.nivel },
        { where: { id: perdedor.id } }
      );
    }

    return { vencedor, perdedor };
  }

  async getPokemonById(id) {
    return PokemonModel.findById(id);
  }

  async getPokemons() {
    return PokemonModel.findAll();
  }

  async savePokemon(pokemon) {
    return PokemonModel.create(pokemon);
  }

  async editPokemon({ treinador }, { id }) {
    await PokemonModel.update(
      { treinador },
      { where: { id: +id } }
    );
    return this.getPokemonById(id);
  }

  async deletePokemon(id) {
    let document = await this.getPokemonById(id);
    return document.destroy();
  }

}

module.exports = new PokemonService();