require('../scripts')();

const serverConfig =  {
  db: {
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    schema: process.env.DB_SCHEMA,

    dialect: process.env.DB_DIALECT,
    dialectOptions: {
      encrypt: true
    },

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  }
};

module.exports = serverConfig;
