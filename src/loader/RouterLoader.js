const path = require('path');
const LoaderHelper = require('../helper/LoaderHelper');
const ServerException = require('../_base/ServerException');
const PATH_ROUTES = path.resolve(__dirname, '../route');

class RouterLoader {

  constructor() {
    this._routesByFileName = {};
  }

  async load() {
    this._routesByFileName = LoaderHelper.loadFiles(PATH_ROUTES);
    let paths = {};

    for (let fileName in this._routesByFileName) {
      let routes = this._routesByFileName[fileName];
      for (let endPointConfig of routes) {
        for (let verb of endPointConfig.verbs) {
          server.app[verb](endPointConfig.route, this.onRequest.bind(this, endPointConfig));

          if (!paths[endPointConfig.route]) {
            paths[endPointConfig.route] = {};
          }

          paths[endPointConfig.route][verb] = {
            "tags": endPointConfig.tags,
            "description": endPointConfig.description,
            "summary": endPointConfig.summary,
            "produces": [
              "application/json",
            ],
            "parameters": endPointConfig.params,
            "responses": endPointConfig.response
          }
        }
      }
    }

    server.app.get('/api-docs', (req, res) => {
      res.json({
        "swagger": "2.0",
        "info": {
          "title": "Pokemon App WS",
          "description": "",
          "version": "1.0.0"
        },
        "responses": {},
        "parameters": {},
        paths,
        definitions: {}
      });
    });
  }

  async onRequest(endPointConfig, req, res) {
    try {
      let {
        Controller,
        declaredMethod,
        params
      } = endPointConfig;
      let requestParams = Object.assign({});
      let pathVariables = req.params || {};

      for (let {schema} of params) {
        for (let key in schema.properties) {
          requestParams[key] = this.validate(key, schema.properties[key], req.getter.get(key));
        }
      }

      let controller = new Controller(req, res);
      let result = await controller[declaredMethod](requestParams, pathVariables);
      if (result === void 0) {
        res.status(204);
      }
      res.sender.end(result);
    } catch (error) {
      res.sender.error(error);
    }
  }

  validate(key, { required, type }, value) {
    if (required) {
      if (value === void 0) {
        throw new ServerException(`Parâmetro: ${key} é obrigatório!`);
      }
    }

    let mappingTypes = {
      'string': (value) => String(value),
      'number': (value) => +value,
      'object': (value) => typeof value === 'string' ? JSON.parse(value) : value,
      'array': (value) => typeof value === 'string' ? JSON.parse(value) : value
    };
    return mappingTypes[type] ? mappingTypes[type](value) : value;
  }

}

module.exports = RouterLoader;