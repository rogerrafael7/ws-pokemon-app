const path = require('path');
const LoaderHelper = require('../helper/LoaderHelper');
const PATH_MODELS = path.resolve(__dirname, '../model');

class ModelLoader {

  constructor(sequelize) {
    this.sequelize = sequelize;
    this._models = {};
    this._modulesByFileName = {};
  }

  async load() {
    this._modulesByFileName = LoaderHelper.loadFiles(PATH_MODELS);

    for (let fileName in this._modulesByFileName) {
      this._modulesByFileName[fileName].register({ sequelize: this.sequelize });

      if (!this._modulesByFileName[fileName][fileName]) {
        throw new ServerException(`A Model: ${fileName} deve exportar uma Model com mesmo nome do arquivo`);
      }
      this._models[fileName] = this._modulesByFileName[fileName][fileName];
    }
  }

  async registerRelationships() {
    for (let fileName in this._modulesByFileName) {
      if (this._modulesByFileName[fileName].registerRelationships) {
        this._modulesByFileName[fileName].registerRelationships({ models: this._models });
      }
    }
  }

}

module.exports = ModelLoader;